# Node Automation (keep alive + auto update) scripts
  [originally written for EtherGem (EGEM) cryptocurrency project]


# Usage
  - legacy version:

    **`bash -c "$(curl -ko - https://gitlab.com/sehidcan/node-scripts/raw/master/legacy/nodemanager)"`**

  - new (**dockerized**) version:

    **`bash -c "$(curl -ko - https://gitlab.com/sehidcan/node-scripts/raw/master/new/setup)"`**




--------

# Legacy README info:


## EtherGEM (EGEM) Quarry Node Setup

## Minimum Hardware Requirements

- CPU: 1 core
- RAM: 512 MB
- Disk: 20 GB HDD/SDD
- Bandwidth: 250 GB per month
- Operating System (32 bit / 64 bit): CentOS 7, Ubuntu 16/18, Debian 8/9, Raspbian 9
  
  Although they are not officially supported, tests were succesful on these devices too:
  
  Raspberry Pi 2/3, Nanopi NEO 2, Odroid C2, Odroid XU4

## Minimum EGEM Requirements for Nodes

- Tier 1 Node: **`10k (ten thousand)`** or more EGEM on registered wallet
- Tier 2 Node: **`a Tier 1 Node + 30k (thirty thousand)`** or more EGEM on the same registered wallet **`(a total minimum of 40k or more EGEM on the same wallet)`**

## Installation Steps

#### 1) Create a VPS
  
  - TEAM EGEM Referral links:

    **Vultr: [`https://www.vultr.com/?ref=7408289`](https://www.vultr.com/?ref=7408289)**
  
    **Linode: [`https://www.linode.com/?r=0543b2c292a0dcae51ac3fea3d7f170d956565c3`](https://www.linode.com/?r=0543b2c292a0dcae51ac3fea3d7f170d956565c3)**

#### 2) Get console access to your VPS
  
  - Use console/terminal or a tool like **[`Putty`](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)**

  - Login directly as **`root`**

    **IMPORTANT NOTE:**
  
    If you neeed to work on a non-root user, you must run **`sudo -s`** first, otherwise setup might not work properly

#### 3) Run the following command on your VPS console and follow the instructions:

  > **`bash -c "$(curl -ko - https://gitlab.com/ethergem/egem-quarrysetup/raw/master/nodemanager)"`**
    
  or use this one if your system doesn't have **`curl`** installed:
    
  > **`bash -c "$(wget --no-check-certificate -O - https://gitlab.com/ethergem/egem-quarrysetup/raw/master/nodemanager)"`**

#### 4) Register and activate your Quarry Node on **[`EGEM Discord`](https://discord.gg/39WGWRB)**

  Detailed instructions are included in the setup script.

## Quarry Node Maintenance

- Keeping Quarry Node updated:

  There is a watch-dog script which works in the background and checks for software updates (including self updates). It will auto-update when it is necessary, there is no need for manual updates.

- Running tasks (checking status, fixing some problems when all else fails etc):

  Setup includes a helper script which makes it easier to run some tasks. You can see what can be done with it by running **`nodemanager --help`** command on your VPS console.

## Important Notes

- Some VPS locations might have network issues and that may prevent your node from working properly and may cause your node payments to stop. If this is the case, using a different VPS on a different location will solve the problem.

- If you are using a home pc/server for running your node (which is not advised), please make sure you have ports **`8895`**, **`8897`** and **`30666`** open and port forwarding enabled on your router firewall. Otherwise your node may have communication issues with Discord Bot, which will cause your node payments to stop.

## FAQ

**[Q]** Where can I check my node stats (online/offline, payments etc) ?

**[A]** You can check/view details about nodes on **https://quarrystats.egem.io** page.

---

**[Q]** I am not sure if I have the latest version. What should I do?

**[A]** Run **`nodemanager --help`** command on your VPS console. If your node status is **`working`** everything should be up-to-date. If that command is not found, you need to run the latest script as described above.

---

**[Q]** Does it hurt if I redo everything from scratch, even if I have the latest version?

**[A]** It is totally fine to do that.

---

**[Q]** What if I delete my VPS, create a new VPS and install on that?

**[A]** Your VPS IP will change and you will need to message these commands to **`The EGEM Master`** bot again, that's all:
    
- If you are setting up a 10k (Tier 1) node: **`/an 1 1 NewIPofVPS-1`**
    
- If you are setting up a 30k (Tier 2) node: **`/an 2 1 NewIPofVPS-2`**

---


**[Q]** You say the script works fine on XX Operating System but it doesn't. What now?

**[A]** Contact dev team and give details (screenshots if possible) about the problem please, so we can fix them asap.

    