#
# variables used here are set in related docker-compose.yml file
#
# --------------------------------------------------------------------------- #
# first container (temporary one to use Go-lang)                              #
# --------------------------------------------------------------------------- #

# use a temporary image for compilation
FROM golang:alpine as golang-container

# set maintainer of this Dockerfile
LABEL maintainer="Şehidcan Erdim <sehidcan@gmail.com>"

# install dependencies
RUN apk add --update --no-cache make gcc musl-dev linux-headers git

# clone Go-EGEM from official repo
RUN git clone --single-branch -b master https://gitlab.com/ethergem/go-egem.git /go-egem

# modify permissions for Go-EGEM directory
RUN chmod 755 -R /go-egem

# run make command for NodeStats
RUN make -C /go-egem stats

# --------------------------------------------------------------------------- #
# second container (main one to store NodeStats)                              #
# --------------------------------------------------------------------------- #

# use a base image for new container
FROM alpine:latest

# copy NodeStats executable from previous Go-lang container
COPY --from=golang-container /go-egem/build/bin/stats /usr/local/bin/

# set port(s) for listening
EXPOSE 8897/tcp
EXPOSE 8897/udp
