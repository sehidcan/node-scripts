#
# variables used here are set in related docker-compose.yml file
#
# --------------------------------------------------------------------------- #
# first container (temporary one to use Go-lang)                              #
# --------------------------------------------------------------------------- #

# use a temporary image for compilation
FROM golang:alpine as golang-container

# set maintainer of this Dockerfile
LABEL maintainer="Şehidcan Erdim <sehidcan@gmail.com>"

# install dependencies
RUN apk add --update --no-cache make gcc musl-dev linux-headers git

# clone Go-EGEM from official repo
RUN git clone --single-branch -b master https://gitlab.com/ethergem/go-egem.git /go-egem

# modify permissions for Go-EGEM directory
RUN chmod 755 -R /go-egem

# run make command for Go-EGEM
RUN make -C /go-egem egem

# --------------------------------------------------------------------------- #
# second container (main one to store Go-EGEM)                                #
# --------------------------------------------------------------------------- #

# use a base image for new container
FROM alpine:latest

# create a mount point at Go-EGEM data directory
VOLUME ["/data"]

# install SSL root certificates
RUN apk add --update --no-cache ca-certificates

# copy Go-EGEM executable from previous Go-lang container
COPY --from=golang-container /go-egem/build/bin/egem /usr/local/bin/

# set port(s) for listening
EXPOSE 8895/tcp
EXPOSE 8895/udp
EXPOSE 30666/tcp
EXPOSE 30666/udp
