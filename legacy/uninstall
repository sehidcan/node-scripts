#!/bin/bash
#
# Uninstall script for EGEM Quarry Nodes
#

show_warning(){
    clear
    sleep 0.5s
    echo
    echo "You are about to remove EGEM Quarry Node completely."
    echo "Press any key to continue or press Ctrl + C to cancel."
    echo
    read key
}

disable_services(){
    for s in "${egem_service}" "${nodestats_service}" "${nodeupdater_service}"
    do
        systemctl disable ${s} &>/dev/null
        systemctl stop ${s} &>/dev/null
    done
}

remove_firewall_rules(){
    if hash iptables &>/dev/null; then
        { iptables -S | sed "/dport 8895/s/-A/iptables -D/e"; } &>/dev/null
        { iptables -S | sed "/dport 8897/s/-A/iptables -D/e"; } &>/dev/null
        { iptables -S | sed "/dport 30666/s/-A/iptables -D/e"; } &>/dev/null
    fi

    if hash firewall-cmd &>/dev/null; then
        default_zone="$(firewall-cmd --get-default-zone)"

        for port in "8895" "8897" "30666"
        do
            firewall-cmd --zone=${default_zone} --permanent --remove-port=${port}/tcp &>/dev/null
            firewall-cmd --zone=${default_zone} --permanent --remove-port=${port}/udp &>/dev/null
        done
    fi

    if hash ufw &>/dev/null; then
        for port in "8895" "8897" "30666"
        do
            ufw delete allow ${port} &>/dev/null
        done
    fi
}

remove_configurations(){
    if [ -f "${HOME}/.bashrc" ]; then
        echo "$(grep -v '/usr/local/go/bin' ${HOME}/.bashrc)" > ${HOME}/.bashrc
    fi

    if [ -f "${HOME}/.profile" ]; then
        echo "$(grep -v '/usr/local/go/bin' ${HOME}/.profile)" > ${HOME}/.profile
    fi

    if [ -f "/etc/profile" ]; then
        echo "$(grep -v '/usr/local/go/bin' /etc/profile)" > /etc/profile
    fi
}

delete_files_folders(){
    for x in "${egemlog}" "${egemlog}.backup" "${sysinfo}" "${pid_file}" \
    "/etc/systemd/system/${egem_service}" "/etc/systemd/system/${nodestats_service}" \
    "/etc/systemd/system/${nodeupdater_service}" \
    "${dir_go_egem}" "${dir_live_net}" "${dir_scripts}" "${temp_dir}" "${chain}" \
    "/usr/bin/${nodemanager}" "/usr/bin/${nodeupdater}" "${golang_dir}" \
    "${HOME}/egem_variables" "/usr/bin/egem" "${HOME}/egem.log" "${HOME}/sysinfo" \
    "${HOME}/node.info" "${HOME}/nodename.egem" "${HOME}/greeter.pid"  "${HOME}/setup.log" \
    "${HOME}/error.log" "${HOME}/autonode" "${HOME}/variables.egem" "${HOME}/nodebookpath.egem" \
    "${HOME}/selfupdater" "${HOME}/egem-net-intelligence-api" "${HOME}/egem-stats-reporting" \
    "/usr/bin/autonode" "/etc/systemd/system/autonode.service"
    do
        echo "Deleting file ---> ${x}"
        rm -rf ${x} &>/dev/null
    done

    systemctl daemon-reload &>/dev/null
}

set_variables(){
    egemlog="${HOME}/egem.log"
    sysinfo="${HOME}/sysinfo"
    pid_file="${HOME}/egem.pid"
    dir_go_egem="${HOME}/go-egem"
    dir_live_net="${HOME}/live-net"
    script_repo_name="egem-quarrysetup"
    dir_scripts="${HOME}/${script_repo_name}"
    temp_dir="${HOME}/egem_temp"
    chain="${HOME}/chaindata.zip"
    golang_dir="/usr/local/go"

    nodemanager="nodemanager"
    nodeupdater="nodeupdater"

    egem_service="egem.service"
    nodestats_service="nodestats.service"
    nodeupdater_service="nodeupdater.service"
}

show_warning
set_variables
disable_services
remove_firewall_rules
remove_configurations
delete_files_folders

echo

exit 0 &>/dev/null
